import requests

class Hello(object):
    def greeting(self, name):
        return "Hello {}!".format(name)

    def get_location(self):
        ip_info_url = "https://ipinfo.io/json"
        r = requests.get(ip_info_url)

        if r.status_code == 200:
            return r.status_code, r.json()

        return r.status_code, {}

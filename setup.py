import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="hello-package",
    version="0.0.2",
    author="Rizal",
    author_email="rizalubuntuuser@gmail.com",
    description="A small example package",
    url="https://gitlab.com/JangIjal/hello",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: Unlicensed",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=["pytest", "requests"],
)

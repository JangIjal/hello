from hello import Hello


def test_greeting():
    hello_obj = Hello()
    assert hello_obj.greeting("Rizal") == "Hello Rizal!"

def test_location():
    hello_obj = Hello()
    status_code, response = hello_obj.get_location()

    assert status_code == 200
    assert isinstance(response, dict)
    for field in ['ip', 'city', 'region', 'country', 'loc', 'org', 'timezone', 'readme']:
        assert field in response
